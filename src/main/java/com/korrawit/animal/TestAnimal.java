/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.animal;

/**
 *
 * @author DELL
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.run();
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is Animal? " + (h1 instanceof Animal));
        System.out.println("h1 is LandAnimal? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is Animal? " + (a1 instanceof Animal));
        System.out.println("a1 is LandAnimal? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is Reptile? " + (a1 instanceof Reptile));

        Cat cat1 = new Cat("Meow");
        cat1.run();
        cat1.eat();
        cat1.walk();
        cat1.speak();
        cat1.sleep();

        Dog dog1 = new Dog("Box");
        dog1.run();
        dog1.eat();
        dog1.walk();
        dog1.speak();
        dog1.sleep();

        Crocodile croc1 = new Crocodile("Croc");
        croc1.crawl();
        croc1.eat();
        croc1.walk();
        croc1.speak();
        croc1.sleep();

        Snake snake1 = new Snake("Larm");
        snake1.crawl();
        snake1.eat();
        snake1.walk();
        snake1.speak();
        snake1.sleep();

        Fish f1 = new Fish("Boo");
        f1.swim();
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();

        Crab crab1 = new Crab("Big Dude");
        crab1.swim();
        crab1.eat();
        crab1.walk();
        crab1.speak();
        crab1.sleep();

        Bat bat1 = new Bat("Dam");
        bat1.fly();
        bat1.eat();
        bat1.walk();
        bat1.speak();
        bat1.sleep();

        Bird bird1 = new Bird("Nok");
        bird1.fly();
        bird1.eat();
        bird1.walk();
        bird1.speak();
        bird1.sleep();

        Plane plane = new Plane("Engine Number 1");
        plane.fly();
        plane.run();
        plane.startEngine();
    }

}
